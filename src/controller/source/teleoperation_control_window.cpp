/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  TeleoperationControlWindow
  @author  Daniel Del Olmo
  @date    02-2018
  @version 3.0
*/

#include "../include/teleoperation_control_window.h"

TeleoperationControlWindow::TeleoperationControlWindow(int argc, char** argv, QWidget* parent)
  : QMainWindow(parent), ui(new Ui::TeleoperationControlWindow)  // initialize ui member
{
  ui->setupUi(this);  // connects all ui's triggers

  // Window always on top

  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);

  setWindowIcon(QIcon(":/images/images/keyboard_control.png"));

  ui->reset_keyboard_button->hide();
  ui->land_keyboard_button->hide();

  active_self_localization_by_odometry = false;

  // Flight time
  this->current_time = new QTime(0, 0, 0);
  setTimerInterval(1000);  // 1 second = 1000
  flight_timer = new QTimer(this);
  flight_timer->start(1000);

  setUp();
  // Layout
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;

  int height = root.get<int>("TELEOPERATION_CONTROL.height");
  int width = root.get<int>("TELEOPERATION_CONTROL.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("TELEOPERATION_CONTROL.position.x"), y0 + root.get<int>("TELEOPERATION_CONTROL.position.y"));

  ui->value_vehicle->setText(QString::fromUtf8(drone_id_namespace.c_str()));
}

TeleoperationControlWindow::~TeleoperationControlWindow()
{
  delete ui;
}

void TeleoperationControlWindow::setTimerInterval(double ms)
{
  d_interval = qRound(ms);
  if (d_interval >= 0)
    d_timerId = startTimer(d_interval);
}

void TeleoperationControlWindow::setUp()
{
  // Manual mode buttons
  QObject::connect(ui->land_keyboard_button, SIGNAL(clicked()), this, SLOT(onLandButton()));
  QObject::connect(ui->take_off_keyboard_button, SIGNAL(clicked()), this, SLOT(onTakeOffButton()));
  QObject::connect(ui->reset_keyboard_button, SIGNAL(clicked()), this, SLOT(onResetCommandButton()));
  QObject::connect(flight_timer, SIGNAL(timeout()), this, SLOT(flightTime()));

  activate_behavior = "request_behavior_activation";
  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("robot_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("initiate_behaviors", initiate_behaviors, "initiate_behaviors");
  n.param<std::string>("behavior_event", behavior_event, "behavior_activation_finished");

  if (!n.getParam("drone_driver_sensor_battery", drone_driver_sensor_battery))
    drone_driver_sensor_battery = "battery";
  if (!n.getParam("wifiIsOk", wifi_connection_topic))
    wifi_connection_topic = "wifiIsOk";

  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace + "/" + list_of_active_behaviors, 1000,
                                      &TeleoperationControlWindow::newBehaviorCallback, this);
  battery_subs = n.subscribe("/" + drone_id_namespace + "/" + drone_driver_sensor_battery, 1,
                             &TeleoperationControlWindow::batteryCallback, this);
  wificonnection_subs = n.subscribe("/" + drone_id_namespace + "/" + wifi_connection_topic, 1,
                                    &TeleoperationControlWindow::wifiConnectionCheckCallback, this);
  n.param<std::string>("window_event_topic", window_event_topic, "window_event");


  // Subscribers
  window_event_sub = n.subscribe("/" + drone_id_namespace + "/" + window_event_topic, 10,
                                   &TeleoperationControlWindow::windowOpenCallback, this);

  // Publishers
  window_event_pub =
      n.advertise<aerostack_msgs::WindowEvent>("/" + drone_id_namespace + "/" + window_event_topic, 1, true);

  // Services
  activate_behavior_srv =
      n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/" + drone_id_namespace + "/" + activate_behavior);
  initiate_behaviors_srv =
      n.serviceClient<droneMsgsROS::InitiateBehaviors>("/" + drone_id_namespace + "/" + initiate_behaviors);

  droneMsgsROS::InitiateBehaviors msg;
  initiate_behaviors_srv.call(msg);

  isAKeyPressed = false;
  is_takenOff = false;

  setFocusPolicy(Qt::StrongFocus);
  acceptedKeys.insert(0x01000012, false);  // Tecla UP
  acceptedKeys.insert(0x01000013, false);  // Tecla DOWN
  acceptedKeys.insert(0x01000014, false);  // Tecla LEFT
  acceptedKeys.insert(0x01000015, false);  // Tecla RIGHT
  acceptedKeys.insert(0x51, false);        // Tecla Q
  acceptedKeys.insert(0x41, false);        // Tecla A
  acceptedKeys.insert(0x54, false);        // Tecla T
  acceptedKeys.insert(0x59, false);        // Tecla Y
  acceptedKeys.insert(0x48, false);        // Tecla H
  acceptedKeys.insert(0x5a, false);        // Tecla Z
  acceptedKeys.insert(0x58, false);        // Tecla X
}


void TeleoperationControlWindow::keyPressEvent(QKeyEvent* e)
{
  QWidget::keyPressEvent(e);
  
  if(!isAKeyPressed && acceptedKeys.contains(e->key()))
  {
    isAKeyPressed = true;
    switch(e->key())
    {
      case Qt::Key_R:
      {
        if(!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x52, true);
          onResetCommandButton();
        }
        break;
      }
      case Qt::Key_T:
      {
        if(!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x54, true);
          onTakeOffButton();
        }
        break;
      }
      case Qt::Key_Y:
      {
        if(!e->isAutoRepeat())
        {
          acceptedKeys.insert(0x59, true);
          onLandButton();
        }
        break;
      }
      case Qt::Key_H:
      {
        acceptedKeys.insert(0x48, true);
        aerostack_msgs::RequestBehaviorActivation::Request msg;
        aerostack_msgs::RequestBehaviorActivation::Response res;
        aerostack_msgs::BehaviorCommandPriority behavior;
        behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
        behavior.priority = 3;
        msg.behavior = behavior;
        activate_behavior_srv.call(msg,res);
        if(!res.ack)
          std::cout << res.error_message << std::endl;
        break;
      }
      case Qt::Key_Right:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000014, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.priority = 3;
          behavior.arguments = "direction: \"RIGHT\"\nspeed: 0.4";
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Left:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000012, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"LEFT\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Down:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000015, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"BACKWARD\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
          break;
      }
      case Qt::Key_Up:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x01000013, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"FORWARD\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
          break;
      }
      case Qt::Key_Q:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x51, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"UP\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_A:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x41, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "KEEP_MOVING_WITH_PID_CONTROL";
          behavior.arguments = "direction: \"DOWN\"\nspeed: 0.4";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_Z:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x5a, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "ROTATE_WITH_PID_CONTROL";
          behavior.arguments = "relative_angle: +179";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
      case Qt::Key_X:
      {
        if(!e->isAutoRepeat() && acceptedKeys.contains(e->key()) && !acceptedKeys.value(e->key()))
        {
          acceptedKeys.insert(0x58, true);
          aerostack_msgs::RequestBehaviorActivation::Request msg;
          aerostack_msgs::RequestBehaviorActivation::Response res;
          aerostack_msgs::BehaviorCommandPriority behavior;
          behavior.name = "ROTATE_WITH_PID_CONTROL";
          behavior.arguments = "relative_angle: -179";
          behavior.priority = 3;
          msg.behavior = behavior;
          activate_behavior_srv.call(msg,res);
          if(!res.ack)
              std::cout << res.error_message << std::endl;
        }
        break;
      }
    }
  }
}

void TeleoperationControlWindow::keyReleaseEvent(QKeyEvent *e)
{
    if(e->isAutoRepeat() || !acceptedKeys.contains(e->key()))
    {
      isAKeyPressed = false;
      e->ignore();
    }
    else if(acceptedKeys.contains(e->key()) && acceptedKeys.value(e->key()))
    {
      acceptedKeys.insert(e->key(), false);
      aerostack_msgs::RequestBehaviorActivation::Request msg;
      aerostack_msgs::RequestBehaviorActivation::Response res;
      aerostack_msgs::BehaviorCommandPriority behavior;
      behavior.name = "KEEP_HOVERING_WITH_PID_CONTROL";
      msg.behavior = behavior;
      behavior.priority = 1;
      if(e->key() != Qt::Key_Y && e->key() != Qt::Key_T && e->key() != Qt::Key_R)
       // activate_behavior_srv.call(msg,res);
      if(!res.ack)
          std::cout << res.error_message << std::endl;
      isAKeyPressed = false;
      QWidget::keyReleaseEvent(e);
    }
    else
    {
      isAKeyPressed = false;
      e->ignore();
      QWidget::keyReleaseEvent(e);
    }
}


void TeleoperationControlWindow::newBehaviorCallback(const droneMsgsROS::ListOfBehaviors& msg)
{
  if (msg.behaviors.size() != 0)
  {
    for (int i = 0; i < msg.behaviors.size(); i++)
    {
      if (msg.behaviors[i] == "TAKE_OFF")
      {
        this->current_time->setHMS(00, 00, 00);
        QString text = this->current_time->toString();
        ui->value_flight_time->setText(text);
        is_takenOff = true;
      }
      else if (msg.behaviors[i] == "LAND")
      {
        this->current_time->setHMS(00, 00, 00);
        QString text = this->current_time->toString();
        ui->value_flight_time->setText(text);
        is_takenOff = false;
      }
      else if (msg.behaviors[i] == "SELF_LOCALIZE_BY_ODOMETRY")
      {
        active_self_localization_by_odometry = true;
      }
    }
  }
}
void TeleoperationControlWindow::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg)
{
  battery_msgs = *msg;
  if (battery_msgs.batteryPercent <= 25.0 && battery_msgs.batteryPercent != 0)
  {
    QPalette* palette = new QPalette();
    palette->setColor(QPalette::WindowText, Qt::red);
    ui->value_batery->setPalette(*palette);
  }
  // Battery
  ui->value_batery->setText(QString::number(battery_msgs.batteryPercent) + "%");
  return;
}

void TeleoperationControlWindow::wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
  is_wifi_connected = msg->data;
  if (is_wifi_connected)
    ui->value_wifi->setText("Connected");
  else
    ui->value_wifi->setText("Disconnected");
}

void TeleoperationControlWindow::flightTime()
{
  if (is_takenOff)
  {
    this->current_time->setHMS(this->current_time->addSecs(+1).hour(), this->current_time->addSecs(+1).minute(),
                               this->current_time->addSecs(+1).second());
    QString text = this->current_time->toString();
    ui->value_flight_time->setText(text);
  }
}

void TeleoperationControlWindow::onTakeOffButton()
{
  

  ui->take_off_keyboard_button->setVisible(false);
  ui->reset_keyboard_button->setVisible(true);
  ui->land_keyboard_button->setVisible(true);

  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority behavior;
  behavior.name = "TAKE_OFF";
  behavior.priority = 3;
  msg.behavior = behavior;
  activate_behavior_srv.call(msg,res);
  if(!res.ack)
    std::cout << res.error_message << std::endl;
}

void TeleoperationControlWindow::onLandButton()
{
  //  isExecuting = false;
  //   std::cout<<"Land pressed button"<<std::endl;

  ui->reset_keyboard_button->setVisible(false);
  ui->land_keyboard_button->setVisible(false);
  ui->take_off_keyboard_button->setVisible(true);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;

  int height = root.get<int>("TELEOPERATION_CONTROL.height");
  int width = root.get<int>("TELEOPERATION_CONTROL.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("TELEOPERATION_CONTROL.position.x"), y0 + root.get<int>("TELEOPERATION_CONTROL.position.y"));
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority behavior;
  behavior.name = "LAND";
  behavior.priority = 3;
  msg.behavior = behavior;
  activate_behavior_srv.call(msg,res);
  if(!res.ack)
    std::cout << res.error_message << std::endl;
}

void TeleoperationControlWindow::onResetCommandButton()
{
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority behavior;
  behavior.name = "ROTATE_WITH_PID_CONTROL";
  behavior.arguments = "angle: 0";
  behavior.priority = 3;
  msg.behavior = behavior;
  activate_behavior_srv.call(msg,res);
  if(!res.ack)
    std::cout << res.error_message << std::endl;
}

void TeleoperationControlWindow::closeEvent(QCloseEvent* event)
{
  window_event_msg.window = aerostack_msgs::WindowEvent::TELEOPERATION_CONTROL;
  window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(window_event_msg);
}

void TeleoperationControlWindow::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void TeleoperationControlWindow::windowOpenCallback(const aerostack_msgs::WindowEvent& msg)
{


  if (msg.window== aerostack_msgs::WindowEvent::PYTHON_CONTROL ||
      msg.window== aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL ||
      msg.window== aerostack_msgs::WindowEvent::BEHAVIOR_TREE_INTERPRETER )
  {
    window_event_msg.window = aerostack_msgs::WindowEvent::TELEOPERATION_CONTROL;
    window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
    window_event_pub.publish(window_event_msg);
    killMe();
  }
  if (msg.window == aerostack_msgs::WindowEvent::INTEGRATED_VIEWER && msg.event == aerostack_msgs::WindowEvent::MINIMIZE)
    showMinimized();
}
