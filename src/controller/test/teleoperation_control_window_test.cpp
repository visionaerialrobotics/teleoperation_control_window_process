/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "teleoperation_control_window.h"
#include <QApplication>
#include <QMessageBox>
#include <cstdio>
#include <gtest/gtest.h>
#include <ros/ros.h>

#include <iostream>

QApplication* app;
TeleoperationControlWindow* w;
int ar;
bool finished = false;
int total_subtests = 0;
int passed_subtests = 0;

void spinnerThread()
{
  while (!finished)
  {
    ros::spinOnce();
  }
}

void displaySubtestResults()
{
  std::cout << "\033[1;33m TOTAL SUBTESTS: " << total_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m SUBTESTS PASSED: " << passed_subtests << "\033[0m" << std::endl;
  std::cout << "\033[1;33m % PASSED: " << (passed_subtests * 100.0) / (total_subtests * 1.0) << "\033[0m" << std::endl;
}

void test()
{
  total_subtests++;
  std::string response;
  std::cout << "\033[1;34m Does a Widget called 'Graphical Teleoperation' appear in the left half of the screen? (Y/N) "
               "\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in position or appearance of the widget\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Does 'drone1' appear as vehicle name? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in receiving vehicle name\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Is 100% the battery charge? (Y/N)\033[0m" << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in receiving battery charge\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Press the 'Start' button. Does 'Flight time' counter start counting time?(Y/N) \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in 'Flight time' counter\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  total_subtests++;
  std::cout << "\033[1;34m Press 'Land' button. Has the  flight timer been set to '00:00:00'? (Y/N)\033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in performing  land using button\033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }
  total_subtests++;
  std::cout << "\033[1;34m Click  the 'Start' button again and then press key 'y' in your keyboard (Graphical "
               "Teleoperation window must be active).  Has the  flight timer been set to '00:00:00'? \033[0m"
            << std::endl;
  getline(std::cin, response);
  EXPECT_TRUE(response == "Y" || response == "y");
  if (response != "Y" && response != "y")
  {
    std::cout << "\033[1;31m Problem in keyboard teleoperation \033[0m\n" << std::endl;
  }
  else
  {
    passed_subtests++;
  }

  displaySubtestResults();

  app->exit();
  finished = true;
}

TEST(GraphicalUserInterfaceTests, keyBoardControlTest)
{
  std::thread thr(&test);
  app->exec();
  thr.join();
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());

  system("bash $AEROSTACK_STACK/launchers/gui_simulated_quadrotor_basic_3.0.sh");
  system("bash $AEROSTACK_STACK/launchers/launcher_simulated_quadrotor_basic_3.0.sh");

  sleep(5);
  ar = argc;
  app = new QApplication(argc, nullptr);
  w = new TeleoperationControlWindow(argc, nullptr);
  w->show();

  std::thread thr(&spinnerThread);

  return RUN_ALL_TESTS();
  thr.join();
}
